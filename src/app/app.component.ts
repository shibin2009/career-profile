import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'career-profile';
  skills = ['Angular', 'Typescript', 'JavaScript', 'HTML5', 'CSS3', 'JQuery','NGRX','RxJs','Ajax','GIT','Azure Devops','GitHub','Jira','VS Code','JAVA','SignalR','GitLab']
}
